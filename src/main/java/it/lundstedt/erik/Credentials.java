package it.lundstedt.erik;
import java.io.File;

import com.fasterxml.jackson.databind.ObjectMapper;





public class Credentials {
		ObjectMapper mapper = new ObjectMapper();
		public String configPath="";

		public Credentials(String rcPath)
		{
		System.out.println(Settings.rcPath);
		this.configPath=rcPath;
		}

		public Credentials() {
			this(Settings.rcPath);
		}

		public User getCredentials()
		{
			try {
				// create object mapper instance
				ObjectMapper mapper = new ObjectMapper();
				File cfgFile=new File(this.configPath);
				User credentials = mapper.readValue(cfgFile, User.class);
				System.out.println(credentials.host);
				return credentials;
			} catch (Exception ex) {
				ex.printStackTrace();

			}
			return null;

		}



}
