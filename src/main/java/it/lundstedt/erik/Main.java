package it.lundstedt.erik;

import jakarta.mail.Address;
import jakarta.mail.Flags;
import jakarta.mail.Folder;
import jakarta.mail.Message;
import jakarta.mail.MessagingException;
import jakarta.mail.Multipart;
import jakarta.mail.NoSuchProviderException;
import jakarta.mail.Part;
import jakarta.mail.Session;
import jakarta.mail.Store;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Properties;

public class Main {

	public static void main(String[] args) {
		// Call method fetch
//		User erik = new Erik();
		Credentials auth=new Credentials();
		User erik=auth.getCredentials();
		fetch(erik);
	}

	public static void fetch(User usr) {
		String pop3Host = usr.host;
		String user = usr.username;
		String password = usr.password;
		try {
			// create properties field
			Properties properties = new Properties();
			properties.put("mail.store.protocol", "pop3");
			properties.put("mail.pop3.host", pop3Host);
			properties.put("mail.pop3.port", "995");
			properties.put("mail.pop3.starttls.enable", "true");
			Session emailSession = Session.getDefaultInstance(properties);
			// emailSession.setDebug(true);
			// create the POP3 store object and connect with the pop server
			Store store = emailSession.getStore("pop3s");
			store.connect(pop3Host, user, password);
			// create the folder object and open it
			Folder emailFolder = store.getFolder("INBOX");
			//Folder readFolder = store.getFolder("INBOX/read");
			emailFolder.open(Folder.READ_WRITE);
			//readFolder.open(Folder.READ_WRITE);
			// retrieve the messages from the folder in an array and print it
			Message[] messages = emailFolder.getMessages();
			//readFolder.appendMessages(messages);
			System.out.println("messages.length---" + messages.length);
			for (int i = 0; i < messages.length; i++) {
				Message message = messages[i];
				System.out.println("---------------------------------");
				writePart(message);
					//message.writeTo(System.out);
					if (Settings.deleteMessageAfter) {
					message.setFlag(Flags.Flag.DELETED, true);
					}
			}
			// close the store and folder objects
			emailFolder.close(true);
			store.close();
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	System.exit(0);
	}
	/*
	 * This method checks for content-type based on which, it processes and fetches
	 * the content of the message
	 */
	public static void writePart(Part p) throws Exception {
		// Call method writeEnvelope
		if (p instanceof Message)
		{
			writeEnvelope((Message) p);
		}
		System.out.println("----------------------------");
		System.out.println("CONTENT-TYPE: " + p.getContentType());
		// check if the content is plain text
		if (p.isMimeType("text/plain")) {
			System.out.println("This is plain text");
			System.out.println("---------------------------");
			// System.out.println((String) p.getContent());
			ContentHandler.messageHandler((String) p.getContent(),p);
			//System.out.println(((Message) p).getSentDate().getTime());
		}
		// check if the content has attachment
		else if (p.isMimeType("multipart/*")) {
			System.out.println("This is a Multipart");
			System.out.println("---------------------------");
			Multipart mp = (Multipart) p.getContent();
			int count = mp.getCount();
			for (int i = 0; i < count; i++)
				writePart(mp.getBodyPart(i));
		}
		// check if the content is a nested message
		else if (p.isMimeType("message/rfc822")) {
			System.out.println("This is a Nested Message");
			System.out.println("---------------------------");
			writePart((Part) p.getContent());
		}
		// check if the content is an inline image
		else if (p.isMimeType("image/jpeg")) {
			System.out.println("--------> image/jpeg");
			Object o = p.getContent();
			InputStream x = (InputStream) o;
			// Construct the required byte array
			System.out.println("x.length = " + x.available());
			byte[] bArray = new byte[x.available()];
			int i = 0;
			while ((i = (int) ((InputStream) x).available()) > 0) {
				int result = (int) (((InputStream) x).read(bArray));
				if (result == -1)
					break;
			}
			System.out.println(i);
			FileOutputStream f2 = new FileOutputStream("/tmp/image.jpg");
			f2.write(bArray);
			x.close();
			f2.close();
		} else if (p.getContentType().contains("image/")) {
			System.out.println("content type" + p.getContentType());
			File f = new File("image" + new Date().getTime() + ".jpg");
			DataOutputStream output = new DataOutputStream(
			new BufferedOutputStream(
			new FileOutputStream(f)));
			com.sun.mail.util.BASE64DecoderStream test = (
			com.sun.mail.util.BASE64DecoderStream)
			p.getContent();
			byte[] buffer = new byte[1024];
			int bytesRead;
			while ((bytesRead = test.read(buffer)) != -1) {
				output.write(buffer, 0, bytesRead);
			}
			output.close();
		} else {
			Object o = p.getContent();
			if (o instanceof String) {
				System.out.println("This is a string");
				System.out.println("---------------------------");
				 System.out.println((String) o);
				//ContentHandler.messageHandler((String) o,null);
			} else if (o instanceof InputStream) {
				System.out.println("This is just an input stream");
				System.out.println("---------------------------");
				InputStream is = (InputStream) o;
				// is = (InputStream) o;
				int c;
				while ((c = is.read()) != -1) {
					System.out.write(c);
				}
				is.close();
			} else {
				System.out.println("This is an unknown type");
				System.out.println("---------------------------");
				System.out.println(o.toString());
			}
		}
	}
	/*
	 * This method would print FROM,TO and SUBJECT of the message
	 */
	public static void writeEnvelope(Message m) throws Exception {
		System.out.println("This is the message envelope");
		System.out.println("---------------------------");
		Address[] a;
		// FROM
		if ((a = m.getFrom()) != null) {
			for (int j = 0; j < a.length; j++)
				System.out.println("FROM: " + a[j].toString());
		}
		// TO
		if ((a = m.getRecipients(Message.RecipientType.TO)) != null) {
			for (int j = 0; j < a.length; j++)
				System.out.println("TO: " + a[j].toString());
		}
		// SUBJECT
		if (m.getSubject() != null)
			System.out.println("SUBJECT: " + m.getSubject());
	}
}
