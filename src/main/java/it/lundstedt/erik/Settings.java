package it.lundstedt.erik;

public class Settings
{
	public static final boolean t=true;
	public static final boolean f=false;
	public static final boolean deleteMessageAfter=t;
   private static final String  usrHome=System.getenv("HOME");
   private static final String  xdgConfigHome=System.getenv("XDG_CONFIG_HOME");
	public static final String  sitemapPath=usrHome+"/gemini/capsule/sitemap.gmi";
	public static final String  rcPath=xdgConfigHome+"/jmr/credentials.json";
	public static final String  capsulePath=usrHome+"/gemini/capsule";

}
