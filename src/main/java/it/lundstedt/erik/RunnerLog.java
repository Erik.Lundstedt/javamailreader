package it.lundstedt.erik;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.github.lalyos.jfiglet.FigletFont;
import jakarta.mail.Message;
import jakarta.mail.MessagingException;
import jakarta.mail.Part;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RunnerLog {
	public String username = "";
	public String title = "";
	// public String titleStyle = "";
	public String settlement = "";
	public String content = "";
	@JsonIgnore
	public String[] bigText;


	String filePathBuilder(String $path, long time) {
		String path = $path+this.settlement.replaceAll(" ", "-").replaceAll("[.][.]", "") + "/"
				+ this.username.replaceAll(" ", "-").replaceAll("[.][.]", "") + "/" + time + ".gmi";
		return path;
	}

	String filelocationBuilder() {
		String path = this.settlement.replaceAll(" ", "-").replaceAll("[.][.]", "") + "/"
		+ this.username.replaceAll(" ", "-").replaceAll("[.][.]", "") + "/";
		return path;
	}

	public void showMessage(Part p) {

		this.content = this.content.replaceAll("#nl#", "\n")
			.replaceAll("  ", " ")
			.replaceAll("  ", " ")
			;
		if (bigText != null ) {
			for (int i = 0; i < bigText.length; i++) {
				try {
					String regx = "\\QbigText[" + i + "]\\E";
					String subst = "```" + bigText[i] + "\n" + FigletFont.convertOneLine(bigText[i]) + "\n```";
					subst=subst.replaceAll("\\\\", "\\\\\\\\");
					final Pattern pattern = Pattern.compile(regx);
					final Matcher matcher = pattern.matcher(this.content);
					this.content = matcher.replaceFirst(subst);
					//System.out.println(this.content);
					// = this.content.replace(regx,
					// "´´´text\\n" + FigletFont.convertOneLine(this.bigText[i]) + "\\n```");
				} catch (IOException e) {
					// Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		System.out.print("user: ");
		System.out.println(this.username);
		String header = "```\n";
		try {
			header += FigletFont.convertOneLine(this.title);
		} catch (IOException e) {
			e.printStackTrace();
			header += this.title;
		}
		header += "```\n";
		System.out.println(this.content);// .replaceAll("#", "\n"));
		long time = System.currentTimeMillis();
		try {
			time = ((Message) p).getSentDate().getTime() + System.currentTimeMillis();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		String path = filePathBuilder(Settings.capsulePath, time);
		Msg.writeFile(path, header, this.content);
		String relPath = filePathBuilder("", time);
		//Msg.addLog(relPath, this.title);
		String fPath=filelocationBuilder();
		Msg.altAddLog(relPath,fPath ,this.title);
	}

} 
