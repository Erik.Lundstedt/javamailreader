package it.lundstedt.erik;
import java.io.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
public class Msg {
	public static void writeFile(String path, String header, String content) {
		try {
			File f = new File(path).getParentFile();
			f.mkdirs();
			FileWriter myWriter = new FileWriter(path);
			myWriter.write(header + "\n");
			myWriter.append("=> /index.gmi index\n\n");
			myWriter.append(content + "\n");
			myWriter.close();
		System.out.println("Successfully wrote to the file at");
		} catch (IOException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}
	}

public static void addLog(String path, String title) {
		System.out.println(path);
		String filePath=Settings.sitemapPath;
		try {
		FileReader myFileReader=new FileReader(filePath);
		BufferedReader myReader = new BufferedReader(myFileReader);
		List<String> lines = new ArrayList<String>();
		String ln;
		lines.add("=> " + path + " " + title.replaceAll(" ", "-"));
		while((ln = myReader.readLine()) != null) {
			lines.add(ln);
		}
		myFileReader.close();
		myReader.close();
		// If you want to convert to a String[]
		String[] data = lines.toArray(new String[]{});
		System.out.println("**********************");
		for (int i = 0; i < data.length; i++) {
			System.out.println(data[i]);
		}
		System.out.println("**********************");
		FileWriter fwNoappend= new FileWriter(filePath, false);
		fwNoappend.write("");
		fwNoappend.close();
		FileWriter fw= new FileWriter(filePath, true);
		PrintWriter myWriter = new PrintWriter(fw);
		for (int i=0;i<data.length;i++) {
				myWriter.println(data[i]);
				System.out.println(data[i]);
			}
			myWriter.close();
			System.out.println("Successfully wrote: " + path + " to the indexing-file at : sitemap.gmi");
		} catch (IOException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}
	}



	public static void altAddLog(String path, String fileLocation, String title) {
		System.out.println(path);
		String filePath=Settings.sitemapPath;
		try {
		FileReader myFileReader=new FileReader(filePath);
		BufferedReader myReader = new BufferedReader(myFileReader);
		List<String> lines = new ArrayList<String>();
		String ln;
		lines.add("=> " + path + " " +fileLocation+title.replaceAll(" ", "-"));
		while((ln = myReader.readLine()) != null) {
			if ( ! ln.contains("Last moddified: ")) {
				lines.add(ln);
			}
		}
		myFileReader.close();
		myReader.close();
		// If you want to convert to a String[]
		String[] data = lines.toArray(new String[]{});
		System.out.println("**********************before");
		for (int i = 0; i < data.length; i++) {
			System.out.println(data[i]);
		}
		System.out.println("**********************after");
		FileWriter fwNoappend= new FileWriter(filePath, false);
		double rnd = Math.floor(Math.random()*10);
		final String timeStamp=LocalDate.now().toString()+" at time: "
			+LocalTime.now().getHour()+":"+LocalTime.now().plusMinutes((int)rnd).getMinute();

		fwNoappend.write("Last moddified: "+timeStamp+"\n");
		fwNoappend.close();
		FileWriter fw= new FileWriter(filePath, true);
		PrintWriter myWriter = new PrintWriter(fw);
		for (int i=0;i<data.length;i++) {
				myWriter.println(data[i]);
				System.out.println(data[i]);
			}
			myWriter.close();
			System.out.println("Successfully wrote: " + path + " to the indexing-file at : sitemap.gmi");
		} catch (IOException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}
	}
}
