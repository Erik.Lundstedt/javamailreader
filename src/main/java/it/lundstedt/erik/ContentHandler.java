package it.lundstedt.erik;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.mail.Part;

public class ContentHandler {

	public static void messageHandler(String message, Part msgObj) {
		ObjectMapper mapper = new ObjectMapper();

	//	System.out.println(message);
		String sanitizedMessage=message.replaceAll("\r", " ")
			.replaceAll("\n", "");

		RunnerLog log = null;
		try {
			//log = mapper.readValue(message, RunnerLog.class);
			log = mapper.readValue(sanitizedMessage, RunnerLog.class);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		log.showMessage(msgObj);

	}
}
