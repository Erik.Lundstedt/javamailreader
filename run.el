;;; run.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021 Erik Lundstedt
;;
;; Author: Erik Lundstedt <https://github.com/erik>
;; Maintainer: Erik Lundstedt <erik@lundstedt.it>
;; Created: september 19, 2021
;; Modified: september 19, 2021
;; Version: 0.0.1
;; Keywords: internal mail
;; Homepage: https://github.com/erik/run
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

       (+tmux/cd-to-project)

(defun run () "Send defined runcommand to running tmux instance."
       (interactive)
(when (string= (system-name) "lubuntupc")
       (+tmux/run "/usr/lib/jvm/java-8-openjdk-amd64/bin/java -javaagent:/home/erik/.local/share/JetBrains/Toolbox/apps/IDEA-C/ch-0/211.7628.21/lib/idea_rt.jar=41469:/home/erik/.local/share/JetBrains/Toolbox/apps/IDEA-C/ch-0/211.7628.21/bin -Dfile.encoding=UTF-8 -classpath /usr/lib/jvm/java-8-openjdk-amd64/jre/lib/charsets.jar:/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/ext/cldrdata.jar:/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/ext/dnsns.jar:/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/ext/icedtea-sound.jar:/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/ext/jaccess.jar:/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/ext/java-atk-wrapper.jar:/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/ext/localedata.jar:/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/ext/nashorn.jar:/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/ext/sunec.jar:/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/ext/sunjce_provider.jar:/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/ext/sunpkcs11.jar:/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/ext/zipfs.jar:/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/jce.jar:/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/jfr.jar:/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/jsse.jar:/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/management-agent.jar:/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/resources.jar:/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/rt.jar:/home/erik/git/projects/java/javaMailReader/target/classes:/home/erik/.m2/repository/com/sun/mail/jakarta.mail/2.0.1/jakarta.mail-2.0.1.jar:/home/erik/.m2/repository/com/sun/activation/jakarta.activation/2.0.1/jakarta.activation-2.0.1.jar:/home/erik/.m2/repository/com/fasterxml/jackson/core/jackson-databind/2.12.4/jackson-databind-2.12.4.jar:/home/erik/.m2/repository/com/fasterxml/jackson/core/jackson-annotations/2.12.4/jackson-annotations-2.12.4.jar:/home/erik/.m2/repository/com/fasterxml/jackson/core/jackson-core/2.12.4/jackson-core-2.12.4.jar:/home/erik/.m2/repository/com/github/lalyos/jfiglet/0.0.8/jfiglet-0.0.8.jar it.lundstedt.erik.Main
"))

(when (string= (system-name) "debianLaptop")
(+tmux/run "/home/erik/.jdks/adopt-openj9-1.8.0_292/bin/java -javaagent:/home/erik/.local/share/JetBrains/Toolbox/apps/IDEA-C/ch-0/212.5284.40/lib/idea_rt.jar=39147:/home/erik/.local/share/JetBrains/Toolbox/apps/IDEA-C/ch-0/212.5284.40/bin -Dfile.encoding=UTF-8 -classpath /home/erik/.jdks/adopt-openj9-1.8.0_292/jre/lib/charsets.jar:/home/erik/.jdks/adopt-openj9-1.8.0_292/jre/lib/ext/cldrdata.jar:/home/erik/.jdks/adopt-openj9-1.8.0_292/jre/lib/ext/dnsns.jar:/home/erik/.jdks/adopt-openj9-1.8.0_292/jre/lib/ext/dtfj.jar:/home/erik/.jdks/adopt-openj9-1.8.0_292/jre/lib/ext/dtfjview.jar:/home/erik/.jdks/adopt-openj9-1.8.0_292/jre/lib/ext/jaccess.jar:/home/erik/.jdks/adopt-openj9-1.8.0_292/jre/lib/ext/localedata.jar:/home/erik/.jdks/adopt-openj9-1.8.0_292/jre/lib/ext/nashorn.jar:/home/erik/.jdks/adopt-openj9-1.8.0_292/jre/lib/ext/sunec.jar:/home/erik/.jdks/adopt-openj9-1.8.0_292/jre/lib/ext/sunjce_provider.jar:/home/erik/.jdks/adopt-openj9-1.8.0_292/jre/lib/ext/sunpkcs11.jar:/home/erik/.jdks/adopt-openj9-1.8.0_292/jre/lib/ext/traceformat.jar:/home/erik/.jdks/adopt-openj9-1.8.0_292/jre/lib/ext/zipfs.jar:/home/erik/.jdks/adopt-openj9-1.8.0_292/jre/lib/jce.jar:/home/erik/.jdks/adopt-openj9-1.8.0_292/jre/lib/jsse.jar:/home/erik/.jdks/adopt-openj9-1.8.0_292/jre/lib/management-agent.jar:/home/erik/.jdks/adopt-openj9-1.8.0_292/jre/lib/resources.jar:/home/erik/.jdks/adopt-openj9-1.8.0_292/jre/lib/rt.jar:/home/erik/git/java/javamailreader/target/classes:/home/erik/.m2/repository/com/sun/mail/jakarta.mail/2.0.1/jakarta.mail-2.0.1.jar:/home/erik/.m2/repository/com/sun/activation/jakarta.activation/2.0.1/jakarta.activation-2.0.1.jar:/home/erik/.m2/repository/com/fasterxml/jackson/core/jackson-databind/2.12.4/jackson-databind-2.12.4.jar:/home/erik/.m2/repository/com/fasterxml/jackson/core/jackson-annotations/2.12.4/jackson-annotations-2.12.4.jar:/home/erik/.m2/repository/com/fasterxml/jackson/core/jackson-core/2.12.4/jackson-core-2.12.4.jar:/home/erik/.m2/repository/com/github/lalyos/jfiglet/0.0.8/jfiglet-0.0.8.jar it.lundstedt.erik.Main
")))


(provide 'run)
;;; run.el ends here

;; Local Variables:
;; mode: emacs-lisp
;; End:
